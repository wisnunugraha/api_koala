BRANCH_TO_MERGE=$(git for-each-ref --format='%(refname:short)' refs/remotes/ |grep -o "DEV_.\+BR$") 
for BRANCH in $BRANCH_TO_MERGE
do 
    echo $BRANCH

    echo $(curl -d source_branch=$CI_BUILD_REF_NAME -d target_branch=$BRANCH -d title=Auto_Merge-Master-to-$BRANCH -d description=default "http://git.xxx.com/api/v3/projects/$CI_PROJECT_ID/merge_requests/?private_token=$GITLAB_API_TOKEN")
done

# Please get project id from triggers.