<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/test', function(){
  return 'Succes';
});

Route::prefix('auth')->middleware('api')->group(function(){
  Route::post('login', 'Api\AuthApiController@LoginAPI');
  Route::post('register', 'Api\AuthApiController@RegisterAPI');
});

Route::prefix('users')->middleware(['jwt.verify','api'])->group(function(){
  Route::post('/orders','Api\DataApiController@OrdersAPI');
  Route::get('/details-orders','Api\DataApiController@DetailsOrdersAPI');
  Route::get('/profile','Api\DataApiController@ProfileAPI');
});

Route::prefix('data')->group(function(){
  Route::get('/payments','Api\DataApiController@PaymentsAPI');
  Route::get('/products','Api\DataApiController@ProductsAPI');
});

// Route Fallback if Route doesn't exist
Route::fallback(function(){
  return response()->json([
      'status'    => false,
      'message'   => 'Page Not Found.',
  ]);
});
