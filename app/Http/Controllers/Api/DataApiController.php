<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Hash;
use JWTAuth;
use JWTFactory;
use Validator;
use DB;
use Auth;

class DataApiController extends Controller
{ 
  // Post data Order  
  public function OrdersAPI(Request $request)
  {
    // return auth()->user();
    if($request->payment_id == '' && $request->products_id == '' && $request->orders_details_qty == '')
    {
      $response = ['message' => 'Data Cannot be null or empty', 'status' => false, 'code' => '00'];
      return response()->json($response);
    }
    if(!is_numeric($request->payment_id))
    {
      $response = ['message' => 'Payment ID must be numeric', 'status' => false, 'code' => '00'];
      return response()->json($response);
    }
    if(!is_numeric($request->products_id))
    {
      $response = ['message' => 'Products ID must be numeric', 'status' => false, 'code' => '00'];
      return response()->json($response);      
    }
    if(!is_numeric($request->orders_details_qty))
    {
      $response = ['message' => 'Quantity ID must be numeric', 'status' => false, 'code' => '00'];
      return response()->json($response);  
    }
    $dateNow = Carbon::now()->format('Y-m-d');
    $monthNow = Carbon::now()->format('m');
    $yearNow = Carbon::now()->format('Y');
    $count = DB::table('orders')->where('orders_date',$dateNow)->count();
    for($i = 0; $i <= $count; $i++)
    {
      $seri = $i;
    }
    if($seri == 0)
    {
      $seri += 1;
    }
    $romawiMonth = $this->RomawiConvert($monthNow);
    $ordersSeri = 'PO-'.$seri.'/'.$romawiMonth.'/'.$yearNow;
    DB::beginTransaction();
    try {
        DB::commit();
        
        $orders_id =  DB::table('orders')
                        ->insertGetId([
                          'orders_number' => $ordersSeri,
                          'orders_date' => $dateNow,
                          'customers_id' => auth()->user()->customers_id,
                          'payment_id' => $request->payment_id,
                          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        ]);
        
        $orders_detail_id = DB::table('orders_details')
                              ->insertGetId([
                                'orders_id' => $orders_id,
                                'products_id' => $request->products_id,
                                'orders_details_qty' => $request->orders_details_qty,
                                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                              ]);
        
        $response = [
                      'message' => 'Order Successfuly',
                      'Order ID'=> $ordersSeri , 
                      'status' => true, 
                      'code' => '01'
                    ];
        return response()->json($response);
        // all good
    } catch (\Exception $e) {
        DB::rollback();
        $response = ['message' => $e, 'status' => false, 'code' => '00'];
        return response()->json($response);
    }
  }


  public function DetailsOrdersAPI()
  {
    $id = auth()->user()->customers_id;
    $result = array();
    $data = DB::table('orders')
              ->where('orders.customers_id', $id)
              ->leftJoin('orders_details','orders_details.orders_id','=','orders.orders_id')
              ->leftJoin('payment','payment.payment_id','=','orders.payment_id')
              ->select([
                        'orders.*',
                        'orders_details.orders_details_qty',
                        'orders_details.products_id',
                        'payment.payment_name',
                        'payment.payment_code',
                      ])
              ->get();
    if(empty($data))
    {
      $response = ['message' => 'Empty', 'status' => false, 'code' => '01'];
      return response()->json($response);

    }
    foreach ($data as $row)
    {
      $product = DB::table('products')->where('products_id', $row->products_id)->first();
      $column['customers_name'] = auth()->user()->customers_name;
      $column['customers_phone'] = auth()->user()->customers_phone;
      $column['customers_email'] = auth()->user()->customers_email;
      $column['orders_id'] = $row->orders_id;
      $column['orders_number'] = $row->orders_number;
      $column['orders_date'] = $row->orders_date;
      $column['orders_quantity'] = $row->orders_details_qty;
      $column['products_name'] = $product->products_name;
      $column['products_price'] = $product->products_price;
      $column['total_price'] = $product->products_price*$row->orders_details_qty;
      $column['payment_name'] = $row->payment_name;      
      $result[] = $column;
    }

    $response = ['message' => 'Successfuly', 'data' => $result,'status' => true, 'code' => '02'];
    return response()->json($response);
  }


  public function ProfileAPI(Type $var = null)
  {
    $result = array();

    $column['customers_name'] = auth()->user()->customers_name;
    $column['customers_email'] = auth()->user()->email;
    $column['customers_phone'] = auth()->user()->customers_phone;
    if(auth()->user()->customers_sex == 1)
    {
      $column['customers_phone'] = 'Male';      
    }
    else
    {
      $column['customers_phone'] = 'female';  
    }
    $result[] = $column;
    $response = ['message' => 'Successfuly', 'data' => $result,'status' => true, 'code' => '02'];
    return response()->json($response);

  }


  public function PaymentsAPI()
  {
    $result = array();
    $data = DB::table('payment')->get();
    foreach ($data as $row)
    {
      $column['payment_id'] = $row->payment_id;
      $column['payment_name'] = $row->payment_name;
      $column['payment_code'] = $row->payment_code;
      $result[] = $column;
    }
    $response = ['message' => 'Successfuly', 'data' => $result,'status' => true, 'code' => '02'];
    return response()->json($response);
  }

  
  public function ProductsAPI()
  {
    $result = array();
    $data = DB::table('products')->get();
    foreach ($data as $row)
    {
      $column['products_id'] = $row->products_id;
      $column['products_name'] = $row->products_name;
      $column['products_price'] = $row->products_price;
      $result[] = $column;
    }
    $response = ['message' => 'Successfuly', 'data' => $result,'status' => true, 'code' => '02'];
    return response()->json($response);
  }



  // Private Function
  private function RomawiConvert($bulan)
  {
    switch ($bulan){
        case 1: 
            return "I";
            break;
        case 2:
            return "II";
            break;
        case 3:
            return "III";
            break;
        case 4:
            return "IV";
            break;
        case 5:
            return "V";
            break;
        case 6:
            return "VI";
            break;
        case 7:
            return "VII";
            break;
        case 8:
            return "VIII";
            break;
        case 9:
            return "IX";
            break;
        case 10:
            return "X";
            break;
        case 11:
            return "XI";
            break;
        case 12:
            return "XII";
            break;
    }    
  }
}
