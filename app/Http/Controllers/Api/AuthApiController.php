<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Hash;
use JWTAuth;
use JWTFactory;
use Validator;
use DB;
use Auth;

class AuthApiController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api', ['except'=> ['LoginAPI','RegisterAPI']]);
  }
  private $token = true;
  private function guard()
  {
    return Auth::guard('api');
  }
  public function LoginAPI(Request $request)
  {
    $input = $request->only('email', 'password');
    $jwt_token = null;
    if (!$jwt_token = auth()->attempt($input)) {
        return response()->json([
            'success' => false,
            'message' => 'Invalid Email or Password',
            'Code' => '00'
        ]);
    }
    return response()->json([
        'token_type' => 'Bearer',
        'access_token' => $this->guard()->attempt($input),
        'expires_in' => $this->guard()->factory()->getTTL(),
        'user' => auth()->user()
    ]);

  }

  
  public function RegisterAPI(Request $request)
  {
    /**
     *  Code API 
     *  1. 00 => Validator True
     *  2. 01 => Email Exists
     *  3. 02 => Phone Number Exists
     */


    $validator = Validator::make($request->all(), [
        'name' => 'required|min:6|max:255',
        'email' => 'required|email|min:6|max:255',
        'phone' => 'required|min:12|max:255',
        'password' => 'required|min:6|max:255',
        'birthday' => 'required|date',
        'sex' => 'required|in:0,1',
    ]);
    if($validator->fails()) 
    {
      $response = ['message' => $validator->errors(), 'status' => false, 'Code' => '00'];
      return response()->json($response);
    }

    // Validate Email Exist
    $check_mail = DB::table('customers')
                    ->where('email', $request->email)
                    ->select('email')
                    ->first();
    if($check_mail)
    {
      $response = ['message' => 'Can not use this email', 'status' => false, 'Code' => '01'];
      return response()->json($response);
    }    
          
    // Validate Phone Number Exist          
    $check_phone =  DB::table('customers')
                      ->where('customers_phone', $request->phone)
                      ->select('customers_phone')
                      ->first();                  
    if($check_phone)
    {
      $response = ['message' => 'Can not use this phone number', 'status' => false, 'Code' => '02'];
      return response()->json($response);
    }

    // Register Data here
    DB::table('customers')
      ->insert([
        'email' => $request->email,
        'password' => Hash::make($request->password),

        'customers_name' => $request->name,
        'customers_phone' => $request->phone,
        'customers_dob' => $request->birthday,
        'customers_sex' => $request->sex,

        'customers_last_login' => Carbon::now()->format('Y-m-d'),
        'customers_last_ip' => $request->ip(),
        'created_at' =>Carbon::now()->format('Y-m-d h:i:s'),
      ]);    

    $result = $this->LoginAPI($request);  
    return $result;
  }

  public function TestAPI()
  {
    // test
  }
}
