<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customers extends Authenticatable implements JWTSubject 
{
    use Notifiable;
    // use HasFactory;

    protected $table = 'customers';
    protected $primaryKey = 'customers_id';
    protected $fillable = [
      'email', 'password',
    ];
 
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
      return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
